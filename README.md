<img align="right" width="200" height="200" src="https://codeberg.org/repo-avatars/15930-179e7386f23fe4a846ed5b93249ad795">

# CLT-Assembly

## Intro
Hier finden sich unsere *CLT-Adventure*-Maps. Dies sind 2D-Karten, die als Basis für die virtuelle Welt *CLT-Adventure* dienen, in der man mit anderen Wesen erstmals während der *Chemnitzer Linux Tage* im Jahr 2021 interagieren konnte und in der wir die *Computertruhe*-Assembly repräsentieren.

Der *CCC* stellt dankenswerterweise ein [Howto](https://howto.rc3.world/maps.html) zur Verfügung, in dem beschrieben steht, wie man solche Karten und auch die hierfür benötigten grafischen Elemente erstellen kann.

*CLT-Adventure* basiert auf *[Work Adventure](https://workadventu.re)*. Wie eine solche 2D-Welt aussehen kann, lässt sich [hier](https://play.staging.workadventu.re/) begutachten.

## Struktur des Repos
### Root
Auf der obersten Ebene liegen die finalen, direkt auslieferbaren Karten im JSON-Format inkl. der referenzierten Assets in den Unterordnern `sounds` und `tilesets`. Wir haben hier die Konvention des *CCC* für den *rC3* übernommen, bei der sich auf dieser Ebene die `main.json`-Einstiegskarte befinden muss.

Außerdem finden sich im Ordner `webpages` kleine, ansprechende Webseiten zum Öffnen in der 2D-Welt. Sie dienen u. a. zur Anzeige von Fotos und diversen Textinformationen.

### Assets
* Alle Sound-Dateien werden unter `/Assets/Sounds` abgelegt.
* Tilesets lagern in separaten Unterordnern im Ordner `/Assets/Tilesets`.