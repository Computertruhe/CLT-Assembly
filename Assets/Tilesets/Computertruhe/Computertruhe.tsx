<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="Computertruhe" tilewidth="32" tileheight="32" tilecount="118" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="Teppich 1.png"/>
 </tile>
 <tile id="1">
  <image width="32" height="32" source="Teppich 2.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="32" source="Teppich 3.png"/>
 </tile>
 <tile id="3">
  <image width="32" height="32" source="Teppich 4.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="Teppich 5.png"/>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="Teppich 6.png"/>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="Teppich 7.png"/>
 </tile>
 <tile id="7">
  <image width="32" height="32" source="Teppich 8.png"/>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="Teppich 9.png"/>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="nix.png"/>
 </tile>
 <tile id="13">
  <image width="32" height="32" source="Fuß (links).png"/>
  <animation>
   <frame tileid="17" duration="2000"/>
   <frame tileid="12" duration="3000"/>
  </animation>
 </tile>
 <tile id="14">
  <image width="32" height="32" source="Fuß (rechts).png"/>
  <animation>
   <frame tileid="12" duration="1000"/>
   <frame tileid="18" duration="2000"/>
   <frame tileid="12" duration="2000"/>
  </animation>
 </tile>
 <tile id="15">
  <image width="32" height="32" source="Fuß (links).png"/>
  <animation>
   <frame tileid="12" duration="2000"/>
   <frame tileid="17" duration="2000"/>
   <frame tileid="12" duration="1000"/>
  </animation>
 </tile>
 <tile id="17">
  <image width="32" height="32" source="Fuß (links).png"/>
 </tile>
 <tile id="18">
  <image width="32" height="32" source="Fuß (rechts).png"/>
 </tile>
 <tile id="22">
  <image width="32" height="32" source="rC3-Logo (1).png"/>
 </tile>
 <tile id="23">
  <image width="32" height="32" source="rC3-Logo (2).png"/>
 </tile>
 <tile id="24">
  <image width="32" height="32" source="rC3-Logo (3).png"/>
 </tile>
 <tile id="25">
  <image width="32" height="32" source="Film 1.png"/>
  <animation>
   <frame tileid="49" duration="100"/>
   <frame tileid="61" duration="100"/>
  </animation>
 </tile>
 <tile id="26">
  <image width="32" height="32" source="Film 2.png"/>
  <animation>
   <frame tileid="50" duration="100"/>
   <frame tileid="62" duration="100"/>
  </animation>
 </tile>
 <tile id="27">
  <image width="32" height="32" source="Film 3.png"/>
  <animation>
   <frame tileid="51" duration="100"/>
   <frame tileid="63" duration="100"/>
  </animation>
 </tile>
 <tile id="28">
  <image width="32" height="32" source="Film 4.png"/>
  <animation>
   <frame tileid="52" duration="100"/>
   <frame tileid="64" duration="100"/>
  </animation>
 </tile>
 <tile id="29">
  <image width="32" height="32" source="Film 5.png"/>
  <animation>
   <frame tileid="53" duration="100"/>
   <frame tileid="65" duration="100"/>
  </animation>
 </tile>
 <tile id="30">
  <image width="32" height="32" source="Film 6.png"/>
  <animation>
   <frame tileid="54" duration="100"/>
   <frame tileid="66" duration="100"/>
  </animation>
 </tile>
 <tile id="31">
  <image width="32" height="32" source="Film 7.png"/>
  <animation>
   <frame tileid="55" duration="100"/>
   <frame tileid="67" duration="100"/>
  </animation>
 </tile>
 <tile id="32">
  <image width="32" height="32" source="Film 8.png"/>
  <animation>
   <frame tileid="56" duration="100"/>
   <frame tileid="68" duration="100"/>
  </animation>
 </tile>
 <tile id="33">
  <image width="32" height="32" source="Film 9.png"/>
  <animation>
   <frame tileid="57" duration="100"/>
   <frame tileid="69" duration="100"/>
  </animation>
 </tile>
 <tile id="34">
  <image width="32" height="32" source="Film 10.png"/>
  <animation>
   <frame tileid="58" duration="100"/>
   <frame tileid="70" duration="100"/>
  </animation>
 </tile>
 <tile id="35">
  <image width="32" height="32" source="Film 11.png"/>
  <animation>
   <frame tileid="59" duration="100"/>
   <frame tileid="71" duration="100"/>
  </animation>
 </tile>
 <tile id="36">
  <image width="32" height="32" source="Film 12.png"/>
  <animation>
   <frame tileid="60" duration="100"/>
   <frame tileid="72" duration="100"/>
  </animation>
 </tile>
 <tile id="49">
  <image width="32" height="32" source="Film 1.png"/>
 </tile>
 <tile id="50">
  <image width="32" height="32" source="Film 2.png"/>
 </tile>
 <tile id="51">
  <image width="32" height="32" source="Film 3.png"/>
 </tile>
 <tile id="52">
  <image width="32" height="32" source="Film 4.png"/>
 </tile>
 <tile id="53">
  <image width="32" height="32" source="Film 5.png"/>
 </tile>
 <tile id="54">
  <image width="32" height="32" source="Film 6.png"/>
 </tile>
 <tile id="55">
  <image width="32" height="32" source="Film 7.png"/>
 </tile>
 <tile id="56">
  <image width="32" height="32" source="Film 8.png"/>
 </tile>
 <tile id="57">
  <image width="32" height="32" source="Film 9.png"/>
 </tile>
 <tile id="58">
  <image width="32" height="32" source="Film 10.png"/>
 </tile>
 <tile id="59">
  <image width="32" height="32" source="Film 11.png"/>
 </tile>
 <tile id="60">
  <image width="32" height="32" source="Film 12.png"/>
 </tile>
 <tile id="61">
  <image width="32" height="32" source="Film 1 (heller).png"/>
 </tile>
 <tile id="62">
  <image width="32" height="32" source="Film 2 (heller).png"/>
 </tile>
 <tile id="63">
  <image width="32" height="32" source="Film 3 (heller).png"/>
 </tile>
 <tile id="64">
  <image width="32" height="32" source="Film 4 (heller).png"/>
 </tile>
 <tile id="65">
  <image width="32" height="32" source="Film 5 (heller).png"/>
 </tile>
 <tile id="66">
  <image width="32" height="32" source="Film 6 (heller).png"/>
 </tile>
 <tile id="67">
  <image width="32" height="32" source="Film 7 (heller).png"/>
 </tile>
 <tile id="68">
  <image width="32" height="32" source="Film 8 (heller).png"/>
 </tile>
 <tile id="69">
  <image width="32" height="32" source="Film 9 (heller).png"/>
 </tile>
 <tile id="70">
  <image width="32" height="32" source="Film 10 (heller).png"/>
 </tile>
 <tile id="71">
  <image width="32" height="32" source="Film 11 (heller).png"/>
 </tile>
 <tile id="72">
  <image width="32" height="32" source="Film 12 (heller).png"/>
 </tile>
 <tile id="73">
  <image width="32" height="32" source="Computertruhe-Tasse.png"/>
 </tile>
 <tile id="74">
  <image width="32" height="32" source="Keks.png"/>
 </tile>
 <tile id="75">
  <image width="32" height="32" source="Kekse.png"/>
 </tile>
 <tile id="76">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="Mate-Kiste.png"/>
 </tile>
 <tile id="77">
  <image width="32" height="32" source="Mate-Kisten 1.png"/>
 </tile>
 <tile id="78">
  <image width="32" height="32" source="Mate-Kisten 2.png"/>
 </tile>
 <tile id="79">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="Mate-Kisten 3.png"/>
 </tile>
 <tile id="80">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="Tux.png"/>
 </tile>
 <tile id="81">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="GNU.png"/>
 </tile>
 <tile id="82">
  <image width="32" height="32" source="Help Wanted.png"/>
 </tile>
 <tile id="84">
  <image width="32" height="32" source="Schriftzug 1.png"/>
 </tile>
 <tile id="85">
  <image width="32" height="32" source="Schriftzug 2.png"/>
 </tile>
 <tile id="86">
  <image width="32" height="32" source="Schriftzug 3.png"/>
 </tile>
 <tile id="87">
  <image width="32" height="32" source="Schriftzug 4.png"/>
 </tile>
 <tile id="88">
  <image width="32" height="32" source="Schriftzug 5.png"/>
 </tile>
 <tile id="89">
  <image width="32" height="32" source="Schriftzug 6.png"/>
 </tile>
 <tile id="90">
  <image width="32" height="32" source="Banner 1.png"/>
 </tile>
 <tile id="91">
  <image width="32" height="32" source="Banner 2.png"/>
 </tile>
 <tile id="93">
  <image width="32" height="32" source="Banner 3.png"/>
 </tile>
 <tile id="100">
  <image width="32" height="32" source="Help Wanted large1.png"/>
 </tile>
 <tile id="101">
  <image width="32" height="32" source="Help Wanted large2.png"/>
 </tile>
 <tile id="102">
  <image width="32" height="32" source="Help Wanted large3.png"/>
 </tile>
 <tile id="103">
  <image width="32" height="32" source="Help Wanted large4.png"/>
 </tile>
 <tile id="104">
  <image width="32" height="32" source="Zielkreis 1.png"/>
 </tile>
 <tile id="105">
  <image width="32" height="32" source="Zielkreis 2.png"/>
 </tile>
 <tile id="106">
  <image width="32" height="32" source="Zielkreis 3.png"/>
 </tile>
 <tile id="107">
  <image width="32" height="32" source="Zielkreis 4.png"/>
 </tile>
 <tile id="108">
  <image width="32" height="32" source="Zielkreis 5.png"/>
 </tile>
 <tile id="109">
  <image width="32" height="32" source="Zielkreis 6.png"/>
 </tile>
 <tile id="110">
  <image width="32" height="32" source="Zielkreis 7.png"/>
 </tile>
 <tile id="111">
  <image width="32" height="32" source="Zielkreis 8.png"/>
 </tile>
 <tile id="112">
  <image width="32" height="32" source="Zielkreis 9.png"/>
 </tile>
 <tile id="113">
  <image width="32" height="32" source="Logo.png"/>
 </tile>
 <tile id="114">
  <image width="32" height="32" source="clapboard.png"/>
 </tile>
 <tile id="115">
  <image width="32" height="32" source="globe.png"/>
 </tile>
 <tile id="116">
  <image width="32" height="32" source="info.png"/>
 </tile>
 <tile id="117">
  <image width="32" height="32" source="no_microphone.png"/>
 </tile>
 <tile id="118">
  <image width="32" height="32" source="no_speaker.png"/>
 </tile>
 <tile id="119">
  <image width="32" height="32" source="no_trespassing.png"/>
 </tile>
 <tile id="120">
  <image width="32" height="32" source="talk.png"/>
 </tile>
 <tile id="122">
  <image width="32" height="32" source="trophy.png"/>
 </tile>
 <tile id="127">
  <image width="32" height="32" source="photo.png"/>
 </tile>
 <tile id="128">
  <image width="32" height="32" source="Pokal.png"/>
 </tile>
 <tile id="129">
  <image width="32" height="32" source="Computerwerk 1.png"/>
 </tile>
 <tile id="130">
  <image width="32" height="32" source="Computerwerk 2.png"/>
 </tile>
 <tile id="131">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="Popcorn.png"/>
 </tile>
 <tile id="136">
  <image width="32" height="32" source="Google_impact_challenge_1.png"/>
 </tile>
 <tile id="137">
  <image width="32" height="32" source="Google_impact_challenge_2.png"/>
 </tile>
 <tile id="138">
  <image width="32" height="32" source="Google_impact_challenge_3.png"/>
 </tile>
 <tile id="139">
  <image width="32" height="32" source="Google_impact_challenge_4.png"/>
 </tile>
 <tile id="144">
  <image width="32" height="32" source="Digitalcourage.png"/>
 </tile>
 <tile id="145">
  <image width="32" height="32" source="Schenklradio.png"/>
 </tile>
 <tile id="157">
  <image width="32" height="32" source="ChCh 1.png"/>
 </tile>
 <tile id="158">
  <image width="32" height="32" source="ChCh 2.png"/>
 </tile>
 <tile id="159">
  <image width="32" height="32" source="ChCh 3.png"/>
 </tile>
 <tile id="160">
  <image width="32" height="32" source="ChCh 4.png"/>
 </tile>
 <tile id="161">
  <image width="32" height="32" source="Computerwerk 1 von 4.png"/>
 </tile>
 <tile id="162">
  <image width="32" height="32" source="Computerwerk 2 von 4.png"/>
 </tile>
 <tile id="163">
  <image width="32" height="32" source="Computerwerk 3 von 4.png"/>
 </tile>
 <tile id="164">
  <image width="32" height="32" source="Computerwerk 4 von 4.png"/>
 </tile>
 <tile id="165">
  <image width="32" height="32" source="Purpur-Tentakel 1.png"/>
 </tile>
 <tile id="166">
  <image width="32" height="32" source="Purpur-Tentakel 2.png"/>
 </tile>
 <tile id="167">
  <image width="32" height="32" source="Purpur-Tentakel.png"/>
 </tile>
 <tile id="168">
  <image width="32" height="32" source="Spritzen.png"/>
 </tile>
</tileset>
