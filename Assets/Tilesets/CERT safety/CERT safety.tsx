<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="test" tilewidth="32" tileheight="32" tilecount="20" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="01_fire_extinguisher.png"/>
 </tile>
 <tile id="1">
  <image width="32" height="32" source="02_kfz_verbandskasten_32x32.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="32" source="03_eh-wandschrank_32x32.png"/>
 </tile>
 <tile id="3">
  <image width="32" height="32" source="04_emergency_exit_left_32x32-sheet.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="05_emergency_exit_right_32x32.png"/>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="06_cert_1818_32x32.png"/>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="07_barrier_tape_mid_32x32.png"/>
 </tile>
 <tile id="7">
  <image width="32" height="32" source="08_barrier_tape_side_32x32-sheet.png"/>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="09_caution_tape_mid_32x32.png"/>
 </tile>
 <tile id="9">
  <image width="32" height="32" source="10_caution_tape_side_32x32.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="collides" type="bool" value="true"/>
  </properties>
  <image width="32" height="32" source="11_safety_cone.png"/>
 </tile>
 <tile id="11">
  <image width="32" height="32" source="12_löschdecke_32x32.png"/>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="13_barrier_tape_mid_corner_32x32-sheet.png"/>
 </tile>
 <tile id="13">
  <image width="32" height="32" source="14_caution_tape_mid_corner_32x32-sheet.png"/>
 </tile>
 <tile id="14">
  <image width="32" height="32" source="15_caution_tape_arrow_32x32-sheet.png"/>
 </tile>
 <tile id="16">
  <image width="32" height="32" source="17_rauchverbot.png"/>
 </tile>
 <tile id="17">
  <image width="32" height="32" source="18_F-schild.png"/>
 </tile>
 <tile id="18">
  <image width="32" height="32" source="19_Stopschild.png"/>
 </tile>
 <tile id="19">
  <image width="32" height="32" source="16_tape_pole_left.png"/>
 </tile>
 <tile id="20">
  <image width="32" height="32" source="16_tape_pole_right.png"/>
 </tile>
</tileset>
