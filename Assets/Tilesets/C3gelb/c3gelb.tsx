<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.3" name="c3gelb" tilewidth="32" tileheight="32" tilecount="7" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="01_disinfection_dispenser_32x32.png"/>
 </tile>
 <tile id="1">
  <image width="32" height="32" source="02_soap_dispenser_32x32.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="32" source="03_dispenser_pole_32x32.png"/>
 </tile>
 <tile id="3">
  <image width="32" height="32" source="04_caution_beware_of_germs_32x32.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="05_caution_biohazard.png"/>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="06_wash_your_hands_32x32.png"/>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="07_wear_your_mask_32x32.png"/>
 </tile>
</tileset>
