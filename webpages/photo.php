<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Computertruhe e. V. - Fotoimpressionen / Photo Impressions</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="icon" href="https://computertruhe.de/wp-content/uploads/2016/06/cropped-Logo-32x32.png" sizes="32x32" />
		<link rel="icon" href="https://computertruhe.de/wp-content/uploads/2016/06/cropped-Logo-192x192.png" sizes="192x192" />
		<link rel="apple-touch-icon" href="https://computertruhe.de/wp-content/uploads/2016/06/cropped-Logo-180x180.png" />
		<meta name="msapplication-TileImage" content="https://computertruhe.de/wp-content/uploads/2016/06/cropped-Logo-270x270.png" />
	</head>

	<body class="clt_body">
		<article>
			<section class="clt_border">
				<h2 class="clt_h2"><a href="https://computertruhe.de/">Computer&#8203;truhe e. V.</a></h2>
				<?php $photoId = preg_match("/^(0\d|1[01])$/", $_GET["id"]) ? $_GET["id"] : "00"; ?>
				<img class="clt_media-fit" src="photos/clt_<?php echo $photoId ?>.jpg">
			</section>
		</article>
		<footer role="contentinfo">
			<p>
				<a class="privacy-policy-link" href="https://computertruhe.de/datenschutzerklaerung/">Datenschutzerklärung</a> | 
				<a href="https://computertruhe.de/impressum">Impressum</a>
			</p>
		</footer>
	</body>
</html>